<?php // vérification de la variable de session
session_start();
require("verif.php");
?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="fontawesome-free-5.11.2-web/css/all.css">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <header>
        <div id="menu" class="navbar">
            <div><a class="lien" href="index.php">Accueil</a></div>
            <div><a class="lien" href="#formation">Liste des produits</a></div>
            <div><a class="lien" href="#competences">Liste des membres</a></div>
            <div><a class="lien" href="membre.html">Se connecter</a></div>
            <div><?php printf("%s", $_SESSION['pseudo']); ?></div>
        </div>

        <div id="meme">
            <a onclick="myFunction()" href="javascript:void(0)"><i id="toggle" class="fas fa-bars fa-4x"></i></a>
            <div id="menu2" class="navbar2">
                <div><a class="lien" href="index.php">Accueil</a></div>
                <div><a class="lien" href="#formation">Liste des produits</a></div>
                <div><a class="lien" href="#competences">Liste des membres</a></div>
                <div><a class="lien" href="membre.html">Se connecter</a></div>
                <div><?php printf("%s", $_SESSION['pseudo']); ?></div>
            </div>
        </div>
        <div id="image_menu"><img id="img_aff" src="image.jpg" alt="Titre">
            <h1 id="titre">Sales Back</h1>
            <button id="inscription"><a href="inscription.php">Inscription</a></button>
        </div>
    </header>
    <section>
        <nav id="prof">
            <ul>
                <li class="lien"><a href="#info_p">Information personnelle</a></li>
                <li class="lien"><a href="#obj">Objet à vendre</a></li>
                <li class="lien"><a href="#memb">Membre</a></li>
            </ul>
        </nav>
        <div id="info_p">
            <label>Prénon :</label><?php printf("%s", htmlspecialchars($_POST['prenom'])); ?>
            <label>Nom :</label><?php printf("%s", htmlspecialchars($_POST['nom'])); ?>
            <label>Date de naissance :</label><?php printf("%s", htmlspecialchars($_POST['dob'])); ?>
            <label>Numéro de téléphone :</label><?php printf("%s", htmlspecialchars($_POST['tel'])); ?>
            <label>E-mail :</label><?php printf("%s", htmlspecialchars($_POST['email'])); ?>
            <label>Pseudo :</label><?php printf("%s", htmlspecialchars($_POST['pseudo'])); ?>
            <img src="" alt="">
        </div>
        <div id="obj">
        </div>
        <div id="memb">
        </div>
    </section>
</body>