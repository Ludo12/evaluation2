<form id="contact" method="post" action="profil.php">
    <fieldset>
        <legend>Inscription</legend>
        <label for="prenom">Prénom :</label>
        <input type="text" id="prenom" name="prenom" required placeholder="ex: Valentin" />
        <label for="nom">Nom :</label>
        <input type="text" id="nom" name="nom" required placeholder="ex: petri"/>
        <br>
        <label for="dob">Date de naissance:</label>
        <input type="date" id="dob" name="dob" value="" min="1900-01-01" max="2020-01-01">
        <!-- min="1900-01-01" max="2020-01-01" -->
        <br><label for="tel">Tel :</label>
        <input type="tel" id="tel" name="tel" required maxlength="10" placeholder="0123456789"/>
        <!-- required maxlength="10" placeholder="0123456789"  -->
        <br><label for="email">Email :</label>
        <input type="mail" id="email" name="email" required maxlength="50" minlength="8" placeholder="aa@aa.fr"/>
        <!-- required maxlength="50" minlength="8" placeholder="aa@aa.fr" -->
        <br><label for="pseudo">Pseudo :</label>
        <input type="string" id="pseudo" name="pseudo" required maxlength="40" minlength="8" placeholder="ex: cocorico07"/>
        <!-- required maxlength="50" minlength="8" placeholder="ex: cocorico07" -->
        <br><label for="password">Mot de passe :</label>
        <input type="password" id="password" name="password" required maxlength="250" minlength="8"/>
        <!-- required maxlength="50" minlength="8" -->
        <br><label for="Confirmation">Confirmation :</label>
        <input type="password" id="confir!ation" name="confirmation" required maxlength="250" minlength="8"/>
        <!-- required maxlength="50" minlength="8" -->
    </fieldset>
    <input type="submit" name="envoi" value="Envoyer" />
</form>
<?php
if (isset($_POST['prenom'])) {
    if (!empty($_POST['prenom'])) {
        $_POST['prenom'] = htmlspecialchars($_POST['prenom']);
    }
}
if (isset($_POST['nom'])) { //si la variable existe  et si elle n'est pas vide  alors je sécurise la valeur
    if (!empty($_POST['nom'])) {
        $_POST['nom'] = htmlspecialchars($_POST['nom']);
    }
}
if (isset($_POST['dob'])) { //si la variable existe  et si elle n'est pas vide  alors je sécurise la valeur
    if (!empty($_POST['dob'])) {
        $_POST['dob'] = htmlspecialchars($_POST['dob']);
    }
}
if (isset($_POST['tel'])) { //si la variable existe  et si elle n'est pas vide  alors je sécurise la valeur
    if (!empty($_POST['tel'])) {
        $_POST['tel'] = htmlspecialchars($_POST['tel']);
    }
}
if (isset($_POST['mail'])) { //si la variable existe  et si elle n'est pas vide  alors je sécurise la valeur
    if (!empty($_POST['mail'])) {
        $_POST['mail'] = htmlspecialchars($_POST['mail']);
    }
}
if (isset($_POST['pseudo'])) { //si la variable existe  et si elle n'est pas vide  alors je sécurise la valeur
    if (!empty($_POST['pseudo'])) {
        $_POST['pseudo'] = htmlspecialchars($_POST['pseudo']);
    }
}
if (isset($_POST['password'])) { //si la variable existe  et si elle n'est pas vide  alors je sécurise la valeur
    if (!empty($_POST['password'])) {
        $_POST['password'] = htmlspecialchars($_POST['password']);
    }
}
if (isset($_POST['confirmation'])) { //si la variable existe  et si elle n'est pas vide  alors je sécurise la valeur
    if (!empty($_POST['confirmation'])) {
        $_POST['confirmation'] = htmlspecialchars($_POST['confirmation']);
    }
}







$test = true;
if (isset($_POST['prenom'])) { //si la variable existe  et si elle n'est pas vide  alors je sécurise la valeur
    $test = $test && (preg_match("[A-Za-z-\s]", $_POST['prenom']));
   
    
} else {
    printf('Caractère non supporté!');
    $test = false;
}
if (isset($_POST['nom'])) { //si la variable existe  et si elle n'est pas vide  alors je sécurise la valeur
    $test = $test && (preg_match("[A-Za-z-\s]", $_POST['nom']));
  
} else {
    printf('Caractère non supporté!');
    $test = false;
}
if (isset($_POST['dob'])) { //si la variable existe  et si elle n'est pas vide  alors je sécurise la valeur
    $test = $test && (date("j, n, Y") == $_POST['dob']);
    
} else {
    printf('Caractère non supporté!');
    $test = false;
}
if (isset($_POST['tel'])) { //si la variable existe  et si elle n'est pas vide  alors je sécurise la valeur
    $test = $test && (preg_match('#(0|\+33)[1-5]( *[0-9]{2}){4}#', $_POST['tel']));

} else {
    printf('Caractère non supporté!');
    $test = false;
}
if (isset($_POST['password'])) { //si la variable existe  et si elle n'est pas vide  alors je sécurise la valeur
    $test = $test && (preg_match("[0-9A-Za-z]", $_POST['password']));
   
} else {
    printf('Caractère non supporté!');
    $test = false;
}
if (isset($_POST['confirmation'])) { //si la variable existe  et si elle n'est pas vide  alors je sécurise la valeur
    
    $test = $test && ((preg_match("[0-9A-Za-z]", $_POST['confirmation']))&&($_POST['confirmation']==$_POST['password']));
} else {
    printf('Caractère non supporté!');
    $test = false;
}
?>