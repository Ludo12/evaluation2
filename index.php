<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="fontawesome-free-5.11.2-web/css/all.css">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <header>
        <div id="menu">
            <div><a class="lien" href="#">Accueil</a></div>
            <div><a class="lien" href="#formation">Liste des produits</a></div>
            <div><a class="lien" href="#competences">Liste des membres</a></div>
            <div><a class="lien" href="membre.html">Se connecter</a></div>
            <div><a class="lien" href="profil.php">Nom</a></div>
        </div>

        <div id="meme">
            <a onclick="myFunction()" href="javascript:void(0)"><i id="toggle" class="fas fa-bars fa-4x"></i></a>
            <div id="menu2" class="navbar2">
                <div><a class="lien" href="#">Accueil</a></div>
                <div><a class="lien" href="#formation">Liste des produits</a></div>
                <div><a class="lien" href="#competences">Liste des membres</a></div>
                <div><a class="lien" href="membre.html">Se connecter</a></div>
                <div><a class="lien" href="#interests">Nom</a></div>
            </div>
        </div>
        <div id="image_menu">
            <img id="img_aff" src="image.jpg" alt="Titre">
            <h1 id="titre">Sales Back</h1>

            <button id="inscription"><a href="inscription.php">Inscription</a></button>
        </div>
    </header>
    <section>
        <div id="accueil">
            <div>
                <p>Notre site est dédié à la site de revente d’objets</p>
                <a href="presentation.html">Voir plus</a>
            </div>
            <div id="block">

                <div id="poupée">
                    <p>Poupée</p><a href="">Détail</a>
                </div>
                <div id="voiture">
                    <p>Voiture télécommandée</p><a href="">Détail</a>
                </div>

                <div id="jeux_video">
                    <p>Jeux vidéo</p><a href="">Détail</a>
                </div>

            </div>
            <div>
                <p>Témoignages de clients satisfaits</p>
                <ul>
                    <li>...</li>
                    <li>...</li>
                    <li>...</li>
                </ul>
            </div>
            <div id="formu">
                <form id="contact" method="post" action="index.php">
                    <fieldset>
                        <legend>Vos coordonnées</legend>
                        <label for="nom">Nom :</label>
                        <input type="text" id="nom" name="nom" maxlength="50" minlength="2" required placeholder="ex: Valentin PETRI" />
                        <label for="email">Email :</label>
                        <input type="mail" id="email" name="email" required maxlength="50" minlength="8" placeholder="aa@aa.fr" />
                    </fieldset>

                    <fieldset>
                        <legend>Votre message :</legend>
                        <label for="objet">Objet :</label>
                        <input type="text" id="objet" name="objet" maxlength="50" required />
                        <label for="message">Message :</label>
                        <textarea id="message" name="message" required maxlength="300" placeholder="Bonjour"></textarea>
                    </fieldset>

                    <input type="submit" name="envoi" value="Envoyer le mail!" />
                </form>
                <?php
                if (isset($_POST['nom'])) {
                    if (!empty($_POST['nom'])) {
                        $_POST['nom'] = htmlspecialchars($_POST['nom']);
                    }
                }
                if (isset($_POST['email'])) { //si la variable existe  et si elle n'est pas vide  alors je sécurise la valeur
                    if (!empty($_POST['email'])) {
                        $_POST['email'] = htmlspecialchars($_POST['email']);
                    }
                }
                if (isset($_POST['objet'])) { //si la variable existe  et si elle n'est pas vide  alors je sécurise la valeur
                    if (!empty($_POST['objet'])) {
                        $_POST['objet'] = htmlspecialchars($_POST['objet']);
                    }
                }
                if (isset($_POST['message'])) { //si la variable existe  et si elle n'est pas vide  alors je sécurise la valeur
                    if (!empty($_POST['message'])) {
                        $_POST['message'] = htmlspecialchars($_POST['message']);
                    }
                }

                ?>

                <?php
                if (!empty($_POST['nom'])  && !empty($_POST['email'])  && !empty($_POST['objet'])  && !empty($_POST['message'])) {
                    printf("Message envoyé");
                    // printf("%s", $_POST['nom']);
                    // printf("%s", $_POST['email']);
                    // printf("%s", $_POST['objet']);
                    // printf("%s", $_POST['message']);

                    mail('valentin.petri@hotmail.fr', $_POST['objet'], $_POST['message'], 'From:' . $_POST['email']);
                } else {
                    printf('toutes les cases ne sont pas remplies');
                }
                ?>
            </div>
        </div>
    </section>


    <footer>
        <p>Infos générales</p>
        <ul>
            <li><a class="lien" href="infog.html">Informations légales sur l’entreprise</a></li>
            <li><a class="lien" href="formulaire.html">Contact</a></li>
            <li><a class="lien" href="gestion.html">Informations sur la politique de gestion des données</a></li>
        </ul>
    </footer>
    <script src="add.js"></script>

</body>

</html>